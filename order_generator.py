# -*- coding: utf-8 -*-
"""
/***************************************************************************
 OrderAoiGenerator
                                 A QGIS plugin
 This plugin generatos AOI to order Planet Images through API
 Generated by Plugin Builder: http://g-sherman.github.io/Qgis-Plugin-Builder/
                              -------------------
        begin                : 2020-01-21
        git sha              : $Format:%H$
        copyright            : (C) 2020 by Christabella Adelina
        email                : christabella.adelina@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
from qgis.PyQt.QtCore import QSettings, QTranslator, QCoreApplication, QVariant
from qgis.PyQt.QtGui import QIcon
from qgis.PyQt.QtWidgets import QAction

from qgis.gui import QgsFileWidget
from qgis.core import QgsProject, QgsMapLayer, QgsVectorLayer, QgsMapLayerProxyModel, QgsField, edit, QgsDistanceArea, QgsVectorFileWriter, QgsFillSymbol, QgsProcessingFeatureSourceDefinition
from qgis import processing

# Initialize Qt resources from file resources.py
from .resources import *
# Import the code for the dialog
from .order_generator_dialog import OrderAoiGeneratorDialog
import os.path


class OrderAoiGenerator:
    """QGIS Plugin Implementation."""

    def __init__(self, iface):
        """Constructor.

        :param iface: An interface instance that will be passed to this class
            which provides the hook by which you can manipulate the QGIS
            application at run time.
        :type iface: QgsInterface
        """
        # Save reference to the QGIS interface
        self.iface = iface
        # initialize plugin directory
        self.plugin_dir = os.path.dirname(__file__)
        # initialize locale
        locale = QSettings().value('locale/userLocale')[0:2]
        locale_path = os.path.join(
            self.plugin_dir,
            'i18n',
            'OrderAoiGenerator_{}.qm'.format(locale))

        if os.path.exists(locale_path):
            self.translator = QTranslator()
            self.translator.load(locale_path)
            QCoreApplication.installTranslator(self.translator)

        # Declare instance attributes
        self.actions = []
        self.menu = self.tr(u'&OrderGenerator')

        # Check if plugin was started the first time in current QGIS session
        # Must be set in initGui() to survive plugin reloads
        self.first_start = None

    # noinspection PyMethodMayBeStatic
    def tr(self, message):
        """Get the translation for a string using Qt translation API.

        We implement this ourselves since we do not inherit QObject.

        :param message: String for translation.
        :type message: str, QString

        :returns: Translated version of message.
        :rtype: QString
        """
        # noinspection PyTypeChecker,PyArgumentList,PyCallByClass
        return QCoreApplication.translate('OrderAoiGenerator', message)


    def add_action(
        self,
        icon_path,
        text,
        callback,
        enabled_flag=True,
        add_to_menu=True,
        add_to_toolbar=True,
        status_tip=None,
        whats_this=None,
        parent=None):
        """Add a toolbar icon to the toolbar.

        :param icon_path: Path to the icon for this action. Can be a resource
            path (e.g. ':/plugins/foo/bar.png') or a normal file system path.
        :type icon_path: str

        :param text: Text that should be shown in menu items for this action.
        :type text: str

        :param callback: Function to be called when the action is triggered.
        :type callback: function

        :param enabled_flag: A flag indicating if the action should be enabled
            by default. Defaults to True.
        :type enabled_flag: bool

        :param add_to_menu: Flag indicating whether the action should also
            be added to the menu. Defaults to True.
        :type add_to_menu: bool

        :param add_to_toolbar: Flag indicating whether the action should also
            be added to the toolbar. Defaults to True.
        :type add_to_toolbar: bool

        :param status_tip: Optional text to show in a popup when mouse pointer
            hovers over the action.
        :type status_tip: str

        :param parent: Parent widget for the new action. Defaults None.
        :type parent: QWidget

        :param whats_this: Optional text to show in the status bar when the
            mouse pointer hovers over the action.

        :returns: The action that was created. Note that the action is also
            added to self.actions list.
        :rtype: QAction
        """

        icon = QIcon(icon_path)
        action = QAction(icon, text, parent)
        action.triggered.connect(callback)
        action.setEnabled(enabled_flag)

        if status_tip is not None:
            action.setStatusTip(status_tip)

        if whats_this is not None:
            action.setWhatsThis(whats_this)

        if add_to_toolbar:
            # Adds plugin icon to Plugins toolbar
            self.iface.addToolBarIcon(action)

        if add_to_menu:
            self.iface.addPluginToMenu(
                self.menu,
                action)

        self.actions.append(action)

        return action

    def initGui(self):
        """Create the menu entries and toolbar icons inside the QGIS GUI."""

        icon_path = ':/plugins/order_generator/icon.png'
        self.add_action(
            icon_path,
            text=self.tr(u'Generate Order AOIs'),
            callback=self.run,
            parent=self.iface.mainWindow())

        # will be set False in run()
        self.first_start = True


    def unload(self):
        """Removes the plugin menu item and icon from QGIS GUI."""
        for action in self.actions:
            self.iface.removePluginMenu(
                self.tr(u'&OrderGenerator'),
                action)
            self.iface.removeToolBarIcon(action)


    def run(self):
        """Run method that performs all the real work"""

        # Save reference to the QGIS project
        self.project = QgsProject.instance()
        self.d = QgsDistanceArea()
        self.d.setEllipsoid('WGS84')

        # Create the dialog with elements (after translation) and keep reference
        # Only create GUI ONCE in callback, so that it will only load when the plugin is started
        if self.first_start == True:
            self.first_start = False
            self.dlg = OrderAoiGeneratorDialog()

        def clip(aoi, bound):
            clipped = ''

            # Get the image ID from bound
            for feature in bound.selectedFeatures():
                name = feature["item_id"]

            clipParams = {   
                'INPUT' : aoi,
                'OUTPUT' : 'memory:' + str(name),
                'OVERLAY' : QgsProcessingFeatureSourceDefinition(bound.id(), True)
            }
                
            clipped = processing.run("native:clip", clipParams)['OUTPUT']

            with edit(clipped):
                clipped.dataProvider().addAttributes([QgsField('item_id', QVariant.String), QgsField('order_area', QVariant.Double)])
                clipped.updateFields()

                for feat in clipped.getFeatures():
                    feat['item_id'] = name
                    feat['order_area'] = round(self.d.measureArea(feat.geometry()), 2)
                    clipped.updateFeature(feat)

                    # print(clipped)
                    # print(feat.attributes())

            # clipped.setAttribute('item_type', bound['item_type'])
            # clipped.setAttribute('time_acquired', bound['acquired'])

            return clipped


        def diff(aoi, remainingAoi, bound):

            diffParams = {   
                'INPUT' : remainingAoi,
                'OUTPUT' : 'memory:buffer',
                'OVERLAY' : bound
            }
            diff = processing.run("native:difference", diffParams)['OUTPUT']

            bufferMinParams = { 
                'DISSOLVE' : False, 
                'DISTANCE' : -0.0002, 
                'END_CAP_STYLE' : 2, 
                'INPUT' : diff,
                'JOIN_STYLE' : 1, 
                'MITER_LIMIT' : 2, 
                'OUTPUT' : 'memory:buffer',
                'SEGMENTS' : 1 
            }
            cleanedDiff = processing.run("native:buffer", bufferMinParams)['OUTPUT']

            bufferParams = { 
                'DISSOLVE' : False, 
                'DISTANCE' : 0.002, 
                'END_CAP_STYLE' : 2, 
                'INPUT' : cleanedDiff,
                'JOIN_STYLE' : 1, 
                'MITER_LIMIT' : 2, 
                'OUTPUT' : 'memory:buffer',
                'SEGMENTS' : 1 
            }
            bufferedAoi = processing.run("native:buffer", bufferParams)['OUTPUT']

            clipParams = {   
                'INPUT' : bufferedAoi,
                'OUTPUT' : 'memory:buffer',
                'OVERLAY' : aoi
            }

            return processing.run("native:clip", clipParams)['OUTPUT']


        def getActiveBounds():
            active = self.iface.mapCanvas().layers()
            bounds = []
            for layer in active:
                if (layer.name() == 'Footprints'):
                    bounds.append(layer)
            return bounds

        def setExport():
            self.dlg.file.setEnabled(self.dlg.exportCheckbox.isChecked())

        # Get all the bounds
        boundLayers = getActiveBounds()
        self.dlg.footprintsCount.setText(str(len(boundLayers)))

        # Set options for Bounds Combo Box to Vector Layers only
        self.dlg.boundsComboBox.setFilters(QgsMapLayerProxyModel.VectorLayer)
        self.dlg.boundsComboBox.setExceptedLayerList(boundLayers)

        # if not (len(boundLayers)>0):
        #     self.dlg.buttonBox.setEnabled(False)

        self.dlg.previewButton.clicked.connect(lambda: print('hola'))
        self.dlg.exportCheckbox.stateChanged.connect(setExport)

        self.dlg.file.setStorageMode(QgsFileWidget.StorageMode.GetDirectory)
        # self.dlg.file.setSelectedFilter()

        # show the dialog
        self.dlg.show()

        # Run the dialog event loop
        result = self.dlg.exec_()
        
        # See if OK was pressed
        if result and len(boundLayers)>0:
            # Get the selected AOI
            aoi = self.dlg.boundsComboBox.currentLayer()
            remainingArea = aoi

            for feature in aoi.getFeatures():
                aoiName = feature["NAME"]

            symbol = QgsFillSymbol.createSimple({'style': 'no', 'color_border': 'red'})

            # Process the selected footprints
            groupName = aoiName + " Order AOIs"
            root = self.project.layerTreeRoot()
            group = root.insertGroup(0, groupName)

            for boundLayer in boundLayers:
                selection = []
                for feature in boundLayer.getFeatures():
                    selection = [feature.id()]
                    boundLayer.selectByIds(selection)

                    # print(feature['item_id'], feature.id())

                    clipped = clip(remainingArea, boundLayer)
                    remainingArea = diff(aoi, remainingArea, clipped)
                    
                    clipped.renderer().setSymbol(symbol)
                    clipped.triggerRepaint()

                    self.project.addMapLayer(clipped, False)                
                    group.addLayer(clipped)

        

            if(self.dlg.exportCheckbox.isChecked()):
                exportPath = self.dlg.file.filePath()
                directory = os.path.join(exportPath, aoiName)
                print("Saving orders in " + directory)
                
                if not os.path.exists(exportPath):
                   print ("Path does not exist")
                else:
                    if not os.path.exists(directory):
                        os.makedirs(directory)
                    for x in group.findLayers():
                        layer = x.layer()
                        error = QgsVectorFileWriter.writeAsVectorFormat(layer, os.path.join(directory, layer.name()), "UTF-8", driverName="GeoJSON")
                        if error[0] == QgsVectorFileWriter.NoError:
                            print (layer.name() + " saved successfully")

            pass


